/* Copyright (c) 2017 e-soa Jacques Desodt, MIT License */
'use strict'

/**
 * TESTS DIRECTLY ON THE MODULE
**/

// Default plugin options
const pluginName = 'rqlite-store'
const role = pluginName + '-test'
const base = null
const entityName = 'test'

// Prerequisites
const httpapi = require('../http-api')
const moduleList = require('../module-list')
const Seneca = require('seneca')
const testFunctions = require('./functions')
const testConfig = require('./config')

// Test prerequisites
const Code = require('code')
const Lab = require('lab')
const lab = (exports.lab = Lab.script())
const before = lab.before
const describe = lab.describe
const it = lab.it
const expect = Code.expect

// Seneca
var seneca
const errMsg = 'ko'
// Mocks the http api
const queryCurrent = httpapi.query
// Mocks the options
const ignoreNoSuchTableErrorCurrent = testConfig.ignore_no_such_table_error

describe('module list', {timeout: testFunctions.timeout}, function () {
  //
  before((fin) => {
    // Sets the Seneca instance
    seneca = testFunctions.setSeneca(Seneca, testConfig, role, fin) // Add 'print' for debug
    fin()
  })
  //
  it('no arg', function (fin) {
    moduleList.list(testConfig, {})
    .catch(function (err) {
      expect(err.error).to.equal(testConfig.badarguments)
      fin()
    })
  })
  //
  it('query: error and not ignore', function (fin) {
    // Mocks the http api
    httpapi.query = function (options, queryString) {
      return new Promise(function (resolve, reject) {
        return reject(new Error(errMsg))
      })
    }
    // Sets the parameter
    const id = '004'
    const entityFactory = seneca.make(base, entityName)
    const entity = entityFactory.make$({id: id})
    const args = { qent: entity }
    //
    moduleList.list(testConfig, args)
    .catch(function (err) {
      httpapi.query = queryCurrent
      expect(err.message).to.equal(errMsg)
      fin()
    })
  })
  //
  it('query: error and ignore', function (fin) {
    testConfig.ignore_no_such_table_error = true
    // Mocks the http api
    httpapi.query = function (options, queryString) {
      return new Promise(function (resolve, reject) {
        return reject({error: testConfig.nosuchtable})
      })
    }
    // Sets the parameter
    const id = '004'
    const entityFactory = seneca.make(base, entityName)
    const entity = entityFactory.make$({id: id})
    const args = { qent: entity }
    //
    moduleList.list(testConfig, args)
    .then(function (result) {
      testConfig.ignore_no_such_table_error = ignoreNoSuchTableErrorCurrent
      httpapi.query = queryCurrent
      expect(result).to.equal([])
      fin()
    })
  })
  //
  it('query: ok', function (fin) {
    const items = [{foo: 'bar'}, {abc: 123}]
    // Mocks the http api
    httpapi.query = function (options, queryString) {
      return new Promise(function (resolve, reject) {
        return resolve(items)
      })
    }
    // Sets the parameter
    const id = '004'
    const entityFactory = seneca.make(base, entityName)
    const entity = entityFactory.make$({id: id})
    const args = { qent: entity, q: {} }
    //
    moduleList.list(testConfig, args)
    .then(function (result) {
      httpapi.query = queryCurrent
      expect(result.length).to.equal(items.length)
      expect(result[0].foo).to.equal('bar')
      expect(result[1].abc).to.equal(123)
      fin()
    })
  })
  //
})
